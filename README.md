# time-machine-project

<h1>Final project for Syneto Liga AC Labs<h2>

Will build on the fintech time machine project we have. We will make a backend api.

<h3>Features required:</h3>
    
    - we can get a list of possible stocks & crypto tickers we can access

    - we can filter the tickers by any of the following: market cap, region, exchange, sector

    - we can save tickers to our portfolio (also we can delete them)

    - we can see a time history on any of the saved tickers

    - we can see the time history of multiple tickers on the same graph (max 5)

    - we can send the graph as an email to someone

    - for a saved ticker I can check the dividends, price to earnings, market cap, high/low for a given period

<h3>Code quality requirements:</h3>
    
    - clean Code
    
    - apply the right design patterns

    - unit testing
    
    - integration/contract testing with the YahooAPI (https://realpython.com/testing-third-party-apis-with-mocks/)
    
    - logging (we should know if something fails and what actions does the app do, also measure how much do commands take)

